# Chrono Particle Format Specification (Version 3 Extensions)

## Changes
Added support for Zstandard compression

Added support for column annotations


## File Format
ChPFv3 headers redefine the formerly reserved 7th and 8th bytes as a `flags` bitfield with the following values:

---
##### **Bit 0:** _reserved_

##### **Bit 1:** `HEADER_KEYS`
If set, the header is immediately followed by a series of null-terminated UTF-8 strings annotating the name of each data column. If the number of strings does not exactly equal the number of columns, the behavior of a compliant parser may be undefined.

##### **Bits 2-15:** _reserved_
---

#### Example C++ Implementation:
```cpp
struct {
	const uint8_t magic {0x43, 0x68, 0x50, 0x46};
	uint16_t version;
	uint16_t flags;
	uint32_t header_size;
	uint32_t num_keys;
	uint16_t types[];
	uint16_t compression_type;
	uint8_t keys[0];
};
```

> Note: All integers are little-endian unless otherwise specified.

7. A 2-byte integer indicating the compression algorithm used for the binary data.

## Compression Types
| **Type** | **Value** | **Description** |
| ---      | ---       | ---             |
| NONE     |         0 | Uncompressed[¹](#footnote-1) |
| ZLIB     |         1 | LZ77 "DEFLATE" compression[²](#footnote-2) |
| ZSTD     |         2 | Zstandard compression |


> <a id="footnote-1">1</a>: An uncompressed ChPFv3 file is partially backwards-compatible with ChPFv1, column annotations will be ignored in earlier versions.

> <a id="footnote-2">2</a>: A ZLIB-compressed ChPFv3 file is partially backwards-compatible with ChPFv2, column annotations will be ignored in earlier versions. 